<?php
require("inc/config.php");
	session_start();
	if ($_SESSION["start"] != true) {
		header("Location: index.php?session-abgelaufen");
	}
?>
<!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<?php
	// Datensatz speichern
	if (isset($_GET['eintragenGeaendert'])) {
		$idProdu = $_GET["eintragenGeaendert"];
		$pname = $_POST['produktename'];
		$beschrieb = $_POST['beschreibung'];
		$anzahl = $_POST['anzahl'];
		$ablaufdate = date('Y-m-d', strtotime($_POST['ablaufdatum']));
		$eingelagertdate = date('Y-m-d', strtotime($_POST['eingelagert']));
		$kaufOrt = $_POST['kaufort'];
		$faCH = $_POST['fach'];
		$updateQuery = "UPDATE produkt SET Produktename = '$pname', Beschreibung = '$beschrieb', Anzahl = '$anzahl', Ablaufdatum = '$ablaufdate', Eingelagert = '$eingelagertdate', Kaufort = '$kaufOrt', Fach = '$faCH' WHERE ID = '$idProdu'";

		$result = mysqli_query($db, $updateQuery);	
		if ($result === FALSE) {
			die(mysql_error());
		} else {
			header("Location: uebersicht.php?succesfully-edit");
		}
	}
?>

<!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->

<?php
	if (isset($_GET['bearbeiten'])){
		// Datensatz abfragen
		$idFromGet = $_GET['bearbeiten'];
		$abfrageQuery ="SELECT ID, Produktename, Beschreibung, Anzahl, Ablaufdatum, Eingelagert, Kaufort, Fach FROM produkt WHERE ID = $idFromGet";
		$result = mysqli_query($db, $abfrageQuery);

		if ($result === FALSE) {
			die(mysql_error());
		}

		$row = mysqli_fetch_array($result);
?>
<head>
  <title>Settings - Produkt bearbeiten</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1 maximum-scale=1, user-scalable=no">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>

<?php include "navigation.php"; ?>
	
	<div class="container">
		<form method="POST" action="eintragaendern.php?eintragenGeaendert=<?php echo $row["ID"]; ?>">
			<div class="form-group">
				<fieldset>
					<label for="produkt">Produktename: </label> <input class="form-control" type="text" name="produktename" value="<?php echo $row["Produktename"];?>" required>
					<label for="beschreibung">Beschreibung: </label> <textarea class="form-control" name="beschreibung" required><?php echo $row["Beschreibung"];?></textarea>
					<label for="anzahl">Anzahl: </label> <input class="form-control" type="text" name="anzahl" min="0" max="10" value="<?php echo $row["Anzahl"];?>" required>
					<label for="ablaufdatum">Ablaufdatum</label> <input class="form-control" type="date" name="ablaufdatum" value="<?php echo date('d.m.Y', strtotime($row["Ablaufdatum"]));?>" required>
					<label for="eingelagert">Eingelagert: </label> <input class="form-control" type="date" name="eingelagert" value="<?php echo date('d.m.Y', strtotime($row["Eingelagert"]));?>" required>
					<label for="kaufort">Kaufort: </label> <input class="form-control" type="text" name="kaufort" value="<?php echo $row["Kaufort"];?>">
					<label for="fach">Fach: </label> <input class="form-control" type="text" name="fach" value="<?php echo $row["Fach"];?>">
				</fieldset>
				<br />
					<button type="submit" class="btn btn-default" name="btnSpeichern">Speichern</button>
					<button type="button" class="btn btn-default" value="back" onClick="history.go(-1);return true;">Abbrechen</button>
			</div>
		</form>
	</div>
<?php
}
?>
