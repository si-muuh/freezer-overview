# README #

#### ChanLOG ####

```
#!python

Version 0.1.0 - 07.06.2015
Erster Upload auf Bitbucket
```


#### ToDo ####
* Studiere die TODO - Datei 


#### Was kann das Script? ####
Wir haben im Keller einen Tiefkühler, der ist voll mit Essensvorräten. Doch der ist im Keller, dunkel und einsam. Wir wissen nicht immer was in welchem Fach bereits vorhanden ist. Mit diesem Script werden wir nie mehr im Einkaufzentrum vor dem Tiefkühlregal stehen und Tiefkühlpizzas einkaufen wenn zuhause im Fach vier noch drei Stück Pizza Funghi sind. Das selbe mit Bratwürsten!

#### Voraussetzung ####
Eigener Webserver mit Apache oder lighttpd, PHP 5 oder neuer und MySQL 4.1 oder neuer.

#### Installation ####
Script herunterladen, entpacken und per FTP auf den Webspace hochladen. MySQL Datenbank erstellen oder die bereits vorhandenen Logindaten (Datenbankserver, Benutzer, Passwort, Datenbankname) in die inc/config.php Datei eintragen.
Die datenbank.sql importieren.
Mit dem Standardbenutzer admin/admin anmelden und anschliessend bei Einstellungen das Passwort ändern.
Es können später auch weitere Benutzer hinzugefügt werden.
Benutzer: admin/admin

#### Lizenz ####
Freezer overview ist lizenziert unter einer Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz.