<?php
session_start();
	if ($_SESSION["start"] != true) {
		header("Location: index.php?session-abgelaufen");
	}

	require("inc/config.php");
	
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Tiefkühler - Über</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1 maximum-scale=1, user-scalable=no">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>


<?php include "navigation.php";?>
  
 
  <div class="container">
  
    <address>
      <strong>Simu</strong><br>
      <abbr title="Telefonnummer">Tel.:</abbr> 078 123 44 55
    </address>

    <address>
      <strong>Simu</strong><br>
      <a href="mailto:siwittwer@posteo.ch">siwittwer [at] posteo [punkt] ch</a>
    </address>

    <?php 
      $version = "0.1.0";
      echo "Freezer Overview Version: ".$version ?>

  </div>

</body>
</html>