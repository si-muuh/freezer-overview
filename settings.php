<?php
require("inc/config.php");
require("inc/functions.php");
session_start();
	if ($_SESSION["start"] != true) {
		header("Location: index.php?session-abgelaufen");
	}
?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<title>Einstellungen</title>
</head>
<body>

	<?php include "navigation.php"; ?>
	
	<?php 
		echo '<div class="container">';
		if(isset($_GET['useradd-erfolgreich'])){
      		echo '<div class="alert alert-success">'
      			.'<a href="settings.php" class="close" data-dismiss="alert" aria-label="close">&times;</a>'
      			.'<strong>Yeahh!</strong> Neuer Benutzer erfolgreich hinzugefügt!'
      			.'</div>';
    	}
    	echo '<div class="container">';
		if(isset($_GET['useredit-erfolgreich'])){
      		echo '<div class="alert alert-success">'
      			.'<a href="settings.php" class="close" data-dismiss="alert" aria-label="close">&times;</a>'
      			.'<strong>Yeahh!</strong> Der benutzer wurde erfolgreich bearbeitet!'
      			.'</div>';
    	}
    	// Benutzerliste anzeigen
		include "userlist.php";
		echo '</div';
  	?> 
	
</body>
</html>