<!DOCTYPE html>
<html lang="de">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1 maximum-scale=1, user-scalable=no">
	<title>Anmelden</title>

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<style>
		form{
			max-width: 425px;
			margin: 0 auto;
		}
	</style>
	<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="/mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
</head>
<body>

	<?php include "navigation.php"; ?>

	 <?php //include "navigation.php"; ?>

	 <div class="container">
				<!-- Meldungen -->
				<?php 
					if(isset($_GET['logout-success'])) {
						echo '<div class="alert alert-success">'
							.'<a href="index.php" class="close" data-dismiss="alert" aria-label="close">&times;</a>'
  							.'<strong>Perfekt!</strong> Du wurdest erfolgreich abgemeldet.'
							.'</div>';
					}
					elseif(isset($_GET['login-fail'])) {
						echo '<div class="alert alert-danger">'
							.'<a href="index.php" class="close" data-dismiss="alert" aria-label="close">&times;</a>'
  							.'<strong>Fehler!</strong> Benutzername oder Passwort falsch.'
							.'</div>';
					}
					
					elseif(isset($_GET['session-abgelaufen'])) {
						echo '<div class="alert alert-danger">'
							.'<a href="index.php" class="close" data-dismiss="alert" aria-label="close">&times;</a>'
  							.'<strong>Fehler!</strong> Bitte zuerst anmelden!!'
							.'</div>';
					}
				?>
				
			</div>
	
	<form method="POST" action="login.php">
		<fieldset id="login">
			<legend>Login</legend>
			<div class="form-group">
				<label for="name">Benutzername: </label> <input type="text" class="form-control" name="benutzername" required placeholder>
			</br>
				<label for="password">Passwort: </label> <input type="password" class="form-control" name="password" required placeholder>
			</br>
				<button type="submit" class="btn btn-default">Submit</button>
			</div>
		</fieldset>
	</form>

</body>
</html>