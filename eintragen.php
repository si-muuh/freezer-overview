<?php
require("inc/config.php");
	if ($_SESSION["start"] != true) {
		header("Location: index.php?session-abgelaufen");
	}

	// In die Datenbank eintragen
	if (isset($_GET['eintragen'])){

		$pname = $_POST['produktename'];
		$beschrieb = $_POST['beschreibung'];
		$anzahl = $_POST['anzahl'];
		$abldate = $_POST['ablaufdatum'];
		// ### Lokales Datum -> MySQL
		$abldate = date('Y-m-d', strtotime($abldate)); 
		$eingedate = $_POST['eingelagert'];
		// ### Lokales Datum -> MySQL
		$eingedate = date('Y-m-d', strtotime($eingedate)); 
		$kaufort = $_POST['kaufort'];
		$fach = $_POST['fach'];
		//ToDo
			// EingetragenVon ergänzen.

		$sqlEintragen  = "INSERT INTO produkt (Produktename, Beschreibung, Anzahl, Ablaufdatum, Eingelagert, Kaufort, Fach)";
		$sqlEintragen .= " VALUES ('$pname','$beschrieb', '$anzahl', '$abldate', '$eingedate', '$kaufort', '$fach')";

		$addrecord = mysqli_query($db, $sqlEintragen);
			if($addrecord == false){
				//echo "Eintrag konnte nicht hinzugefügt werden!!";
				header("Location: uebersicht.php?failed-entry");
				die();
			}
		mysqli_close($db);
		header("Location: uebersicht.php?succesfull-entry");
		die();
	}
	

?>

	<form method="POST" action="eintragen.php?eintragen">
		<div class="form-group">
			<fieldset>
				<label for="produkt">Produktename: </label> <input class="form-control" type="text" name="produktename" required>
				<label for="beschreibung">Beschreibung: </label> <textarea class="form-control" name="beschreibung" required></textarea>
				<label for="anzahl">Anzahl: </label> <input class="form-control" type="text" name="anzahl" min="0" max="10" required>
				<label for="ablaufdatum">Ablaufdatum</label> <input class="form-control" type="date" name="ablaufdatum" required>
				<label for="eingelagert">Eingelagert: </label> <input class="form-control" type="date" name="eingelagert" required>
				<label for="kaufort">Kaufort: </label> <input class="form-control" type="text" name="kaufort">
				<label for="fach">Fach: </label> <input class="form-control" type="text" name="fach">
			</fieldset>
			<br />
				<button type="submit" class="btn btn-default">Speichern</button>
				<button type="reset" class="btn btn-default">Leeren</button>
		</div>
		
	</form>