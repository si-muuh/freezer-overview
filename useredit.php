<?php 
require("inc/config.php");
session_start();
if ($_SESSION['start'] != 'true') {
    header("Location: index.php?session-abgelaufen");
    }
?>
<!-- //////////////////////////////////////////////////////////////////////////////////////// -->
<?php 

	if (isset($_GET['useredit-erfolgreich'])){
		$idfuerQuery = $_SESSION["idFromGet"];
		$bnameNeu = $_POST['username'];
		$nameNeu = $_POST['name'];
		$emailNeu = $_POST['mail'];

		if (isset($_POST['istadmin'])) {
			$istadminNeu = 1;
		} else {
			$istadminNeu = 0;
		}

		$userUpdateQuery = "UPDATE tk_user SET username='$bnameNeu', Name='$nameNeu', mail='$emailNeu', isroot='$istadminNeu' WHERE id = '$idfuerQuery' ";

		$result = mysqli_query($db, $userUpdateQuery);	
		if ($result === FALSE) {
			die(mysql_error());
		} else {
			header("Location: settings.php?useredit-erfolgreich");
		}
	}
?>
<!-- /////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<?php

	if (isset($_GET['editBenutzer'])){
	    $_SESSION["idFromGet"] = $_GET['editBenutzer'];
		$idfuerQuery = $_SESSION["idFromGet"];

		$userAbfrageQuery = "SELECT username, Name, mail, isroot FROM tk_user WHERE id = '$idfuerQuery'";

		$result = mysqli_query($db, $userAbfrageQuery);	
			if ($result === FALSE) {
				die(mysql_error());
			}

		$row = mysqli_fetch_array($result);

		// Falls der Benutzer den status isroot = 1 hat, wir die checkbox aktiviert.
		if ($row['isroot'] == 1) {
			$isChecked = "checked='checked'";
		} else {
			$isChecked = "";
		}
?>

<head>
  <title>Settings - Benutzer bearbeiten</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1 maximum-scale=1, user-scalable=no">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>

<?php include "navigation.php"; ?>

<div class="container">
    <form action="useredit.php?useredit-erfolgreich" id="user" method="POST">
    <h2>Benutzer editieren</h2>
        <fieldset id="login">
        	<legend>User:</legend>
        
		        <div class="form-group">
		            <label for="user">Username: </label>
		            <input class="form-control" type="text" name="username" id="user" value="<?php echo $row["username"];?>" required>
		        </div>
		        <div class="form-group">
		            <label for="mail">Vorname: </label>
		            <input class="form-control" type="mail" name="name" placeholder="Vorname" value="<?php echo $row["Name"];?>" required>
		        </div>
		        <div class="form-group">
		            <label for="mail">E-Mail: </label>
		            <input class="form-control" type="mail" name="mail" placeholder="Email" value="<?php echo $row["mail"];?>" required>
		        </div>
		        <div class="form-group">
		            <label class="checkbox-inline"><input type="checkbox" name="istadmin" value="placeholder" <?php echo $isChecked ?>>Ist Admin</label> 
		            <a href="#" data-toggle="tooltip" title="Der Admin hat ein paar Einstellungen mehr!"><span class="glyphicon glyphicon-info-sign"></span></a>
		        </div>
		        <div class="form-group">
		            <button type="submit" class="btn btn-primary">Speichern</button>
		    	</div>
        </fieldset>
    </form>
 </div>
<?php
} // Schliesst die if Bedinung
?>