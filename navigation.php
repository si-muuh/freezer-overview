<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>

  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="admin.php">Tiefkühler</a>
      </div>
      <div>
        <ul class="nav navbar-nav">
          <li><a href="uebersicht.php">Übersicht</a></li>
          <?php if (@$_SESSION["istroot"] == 1){ echo "<li><a href='settings.php'>Einstellungen</a></li>";} ?>
          <li><a href="about.php">Über</a></li>
          <li><a href="logout.php">Logout<?php if(isset($_SESSION["benutzername"])) echo " - ".$_SESSION["benutzername"] ;?></a></li>

        </ul>
      </div>
    </div>
  </nav>
</body>
</html>