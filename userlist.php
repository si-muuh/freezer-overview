<?php
require("inc/config.php");
	//session_start();
	if ($_SESSION["start"] != true) {
		header("Location: index.php?session-abgelaufen");
	}

?>

	<?php 
		$befehl = "SELECT id, username, mail, LastLogin FROM tk_user WHERE username IS NOT NULL";
        $result = $db->query($befehl);

    ?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Einstellungen</title>
</head>
<body>
	
	<div class="container">
		<h4>Benutzer verwaltung</h4>
		<table class="table">
			<?php
	          	echo "<th>"."ID"."</th>"
	              ."<th>"."Benutzername"."</th>"
	              ."<th>"."E-Mail"."</th>"
	              ."<th>"."Letzter Login"."</th>"
	              ."<th>"."Einstellungen"."</th>";

	          	while($row = $result->fetch_assoc()) {
	            	echo "<tr>";
	            	echo "<td>"."{$row['id']}"."</td>"
	                	."<td>"."{$row['username']}"."</td>"
	                	."<td>"."{$row['mail']}"."</td>"
	                	."<td>"."{$row['LastLogin']}"."</td>"
	                	."<td>";?> 
	               	<!-- Edit Button hinzufügen -->
                	<a href="useredit.php?editBenutzer=<?php echo $row['id'];?>" onclick="return confirm('Möchten Sie diesen Benutzer wirklich bearbeiten?');"> <button type="button" class="btn btn-default btn-sm" name="edit" id="edit"> <span class="glyphicon glyphicon-edit"></span> Edit </button>
                	<!-- Remove Button hinzufügen -->
	                <a href="delete.php?deleteBenutzer=<?php echo $row['id'];?>" onclick="return confirm('Möchten Sie diesen Benutzer wirklich löschen?');"> <button type="button" class="btn btn-default btn-sm" name="delete" id="delete"> <span class="glyphicon glyphicon-trash"></span> Trash </button>
	                
	        <?php echo "</td>";
	            }
	        ?>
	    </table>
		<button type="button" class="btn btn-default" data-toggle="modal" onclick="location.href='useradd.php'">Neuer Benutzer</button>
	
    </div>
	
</body>
</html>