<?php
require("inc/config.php");
require("inc/functions.php");
session_start();
	if ($_SESSION["start"] != true) {
		header("Location: index.php?session-abgelaufen");
	}
?>


<!DOCTYPE html>
<html lang="en">
<head>
  <title>Tiefkühler - Übersicht</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>


<?php include "navigation.php"; ?>

<!--  Hier werden Meldungen ausgegeben 
       -->
<?php 
  echo '<div class="container">';
    // Meldung wenn das Produkt erfolgreich gelöscht wurde //
      if(isset($_GET['loeschen-success'])) {
          echo '<div class="container">'
             .'<div class="alert alert-success">'
              .'<a href="uebersicht.php" class="close" data-dismiss="alert" aria-label="close">&times;</a>'
              .'<strong>Perfekt!</strong> Das Produkt wurde gelöscht!'
             .'</div>';
          }
    // Meldung wenn das Produkt erfolgreich eingetragen wurde //
      elseif(isset($_GET['succesfull-entry'])) {
          echo '<div class="alert alert-success">'
              .'<a href="uebersicht.php" class="close" data-dismiss="alert" aria-label="close">&times;</a>'
              .'<strong>Yeahh!</strong> Produkt wurde erfolgreich eingetragen!'
              .'</div>';
      }
    // Meldung wenn bei Eintragen einen Fehler passierte //
      elseif(isset($_GET['failed-entry'])) {
          echo '<div class="alert alert-danger">'
              .'<a href="uebersicht.php" class="close" data-dismiss="alert" aria-label="close">&times;</a>'
              .'<strong>Fehler!</strong> Beim eintragen des Produkts hat sich einen Fehler eingeschlichen!'
              .mysql_error()
              .'</div>';
      }
    // Produkt konnte erfolgreich editiert werden //
      elseif(isset($_GET['succesfully-edit'])) {
          echo '<div class="alert alert-success">'
              .'<a href="uebersicht.php" class="close" data-dismiss="alert" aria-label="close">&times;</a>'
              .'<strong>Yeahh!</strong> Produkt wurde erfolgreich editiert!'
              .mysql_error()
              .'</div>';
      }
    // Fehler beim editieren des Produkts //
      elseif(isset($_GET['failed-edit'])) {
          echo '<div class="alert alert-danger">'
              .'<a href="uebersicht.php" class="close" data-dismiss="alert" aria-label="close">&times;</a>'
              .'<strong>Fehler!</strong> Beim editieren des Produkts hat sich einen Fehler eingeschlichen!'
              .mysql_error()
              .'</div>';
      }
  echo '</div>';
?>
  
<div class="container">
  <h3>Einträge</h3>

  <p><?php $befehl = "SELECT ID, Produktename, Anzahl, Beschreibung, Ablaufdatum, Eingelagert, Kaufort, Fach FROM produkt WHERE Produktename IS NOT NULL";
          $result = $db->query($befehl);

          ?><table class="table">
          <?php
          echo "<th>"."Produktename"."</th>"
              ."<th>"."Anzahl"."</th>"
              ."<th>"."Beschreibung"."</th>"
              ."<th>"."Ablaufdatum"."</th>"
              ."<th>"."Eingelagert"."</th>"
              ."<th>"."Kaufort"."</th>"
              ."<th>"."Fach"."</th>"
              ."<th>"."Bearbeiten"."</th>";
          while($row = $result->fetch_assoc()) {
            //$datum_deutsch = date('d.m.Y', strtotime($row['Ablaufdatum']));
            echo "<tr>";
            echo "<td>"."{$row['Produktename']}"."</td>"
                ."<td>"."{$row['Anzahl']}"."</td>"
                ."<td>"."{$row['Beschreibung']}"."</td>"
                ."<td>".date('d.m.Y', strtotime($row['Ablaufdatum']))."</td>"
                ."<td>".date('d.m.Y', strtotime($row['Eingelagert']))."</td>"
                ."<td>"."{$row['Kaufort']}"."</td>"
                ."<td>"."{$row['Fach']}"."</td>"
                ."<td>";?>
            <!-- Edit Button hinzufügen -->
                <a href="eintragaendern.php?bearbeiten=<?php echo $row['ID']; ?>" onclick="return confirm('Möchten Sie diesen Eintrag wirklich bearbeiten?');"> <button type="button" class="btn btn-default btn-sm" name="delete" id="delete"> <span class="glyphicon glyphicon-edit"></span> Edit </button> 
            <!-- Remove Button hinzufügen -->
                <a href="delete.php?deleteEintrag=<?php echo $row['ID'];?>" onclick="return confirm('Möchten Sie diesen Eintrag wirklich löschen?');" >  <button type="button" class="btn btn-default btn-sm" name="delete" id="delete"> <span class="glyphicon glyphicon-trash"></span> Trash </button>


      <?php
            echo "</tr>";
          } 
            echo "</table>";
      ?>
 <!-- // ### MySQL -> Lokales Datum
$datum_deutsch = date('d.m.Y', strtotime($row['Datum'])); -->
  

  <!-- Trigger the modal with a button -->
<button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal">Neuer Eintrag</button>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Neues Produkt eintragen</h4>
      </div>
      <div class="modal-body">
        <!-- Eintragen Formular -->
        <p><?php include "eintragen.php"; ?></p>
        <!-- Eintragen Formular Ende -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</div>

</body>
</html>
