<?php 
require("inc/config.php");
session_start();
if ($_SESSION['start'] != 'true') {
    header("Location: index.php?session-abgelaufen");
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Tiefkühler - Admin</title>
  <meta charset="utf-8">

</head>
<body>

    <?php include "navigation.php" ?>
  
<div class="container">
    <form action="" id="user" method="post">
    <h2>Benutzer erfassen</h2>
        <fieldset id="login">
        <legend>User:</legend>
        
        <div class="form-group">
            <label for="user">Username: </label>
            <input class="form-control" type="text" name="username" id="user" placeholder="Username" required>
        </div>
        <div class="form-group">
            <label for="mail">Vorname: </label>
            <input class="form-control" type="mail" name="name" placeholder="Vorname" required>
        </div>
        <div class="form-group">
            <label for="mail">E-Mail: </label>
            <input class="form-control" type="mail" name="mail" placeholder="Email" required>
        </div>
        <div class="form-group">
            <label for="pw">Passwort: </label>
            <input class="form-control" type="password" name="passwort" placeholder="Passwort" required>
        </div>
        <div class="form-group">
            <label class="checkbox-inline"><input type="checkbox" name="istadmin" value="istadmin">Ist Admin</label> 
            <a href="#" data-toggle="tooltip" title="Der Admin hat ein paar Einstellungen mehr!"><span class="glyphicon glyphicon-info-sign"></span></a>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Speichern</button>
    </div>
        </fieldset>
        </form>

            <!-- jQuery -->
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
        });
    </script>


<?php 
if (!empty($_POST)) //ueberprueft ob der submit button gedrückt wurde
{
    $username = htmlspecialchars(trim($_POST['username']));  
    $mail = htmlspecialchars(trim($_POST['mail'])); 
    $passwort = htmlspecialchars(trim($_POST['passwort']));
    $vorname = htmlspecialchars(trim($_POST['name']));
    $salt = uniqid(mt_rand(), true);
    if (isset($_POST['istadmin'])) {
        $istadminQuery = 1;
    } else {
        $istadminQuery = 0;
    }
    $sql = "INSERT INTO tk_user (username, Name, passwort, salt, mail, isroot) VALUES ('$username', '$vorname', SHA1('$passwort'), '$salt', '$mail', '$istadminQuery')";

    if ($db->query($sql) === TRUE) {
        //echo "neuer User angelegt!";
        header("Location: settings.php?useradd-erfolgreich");
        die();
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
        }
}
?>
    </div> 
    </body>
    </html>