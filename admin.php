<?php
require("inc/config.php");
require("inc/functions.php");
session_start();
	if ($_SESSION["start"] != true) {
		header("Location: index.php?session-abgelaufen");
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Tiefkühler - Admin</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1 maximum-scale=1, user-scalable=no">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>


<?php include "navigation.php"; ?>
  
  <div class="container">

   <?php 

     if(isset($_GET['login-success'])) {
            echo '<div class="alert alert-success">'
              .'<a href="admin.php" class="close" data-dismiss="alert" aria-label="close">&times;</a>'
              .'<strong>Perfekt!</strong> Du wurdest erfolgreich angemeldet. Hallo '.$_SESSION["benutzername"]
              .'</div>';
      }
    ?> 
  </div>

  <div class="container">
    <h4>Ist bereits abgelaufen</h4>

    <?php $ablaufdatum0Tage = "SELECT Produktename, Ablaufdatum, Fach, DATEDIFF(Ablaufdatum,CURDATE()) AS DiffDate FROM produkt WHERE DATEDIFF(Ablaufdatum,CURDATE()) < 0 ";
            $result = $db->query($ablaufdatum0Tage);
            if (empty($result)){
              echo "Alles leer";
            }
            ?><table class="table">
            <?php
              echo "<th>"."Produktename"."</th>"
                ."<th>"."Ablaufdatum"."</th>"
                ."<th>"."Fach"."</th>"
                ."<th>"."Tage"."</th>";
            while($row = $result->fetch_assoc()) {
              echo "<tr>";
              echo "<td>"."{$row['Produktename']}"."</td>"
                  ."<td>".date('d.m.Y', strtotime($row['Ablaufdatum']))."</td>"
                  ."<td>"."{$row['Fach']}"."</td>"
                  ."<td>"."{$row['DiffDate']}"."</td>"
                  ."</tr>";
            } 
              echo "</table>";
    ?>
  </div>

  <div class="container">
    <h4>Läuft innerhalb 30 Tage ab!</h4>

    <?php 
            $tageFilterZwei = 30;
            $ablaufdatum30Tage = "SELECT Produktename, Ablaufdatum, Fach, DATEDIFF(Ablaufdatum,CURDATE()) AS"
                                ." Laeuftabin FROM produkt WHERE DATEDIFF(Ablaufdatum,CURDATE()) < '$tageFilterZwei' AND DATEDIFF(Ablaufdatum,CURDATE()) > 0";
            $result = $db->query($ablaufdatum30Tage);

            ?><table class="table">
            <?php
              echo "<th>"."Produktename"."</th>"
                ."<th>"."Ablaufdatum"."</th>"
                ."<th>"."Fach"."</th>"
                ."<th>"."Tage"."</th>";
            while($row = $result->fetch_assoc()) {
              echo "<tr>";
              echo "<td>"."{$row['Produktename']}"."</td>"
                  ."<td>".date('d.m.Y', strtotime($row['Ablaufdatum']))."</td>"
                  ."<td>"."{$row['Fach']}"."</td>"
                  ."<td>"."{$row['Laeuftabin']}"."</td>"
                  ."</tr>";
            } 
              echo "</table>";
    ?>
  </div>
</body>
</html>